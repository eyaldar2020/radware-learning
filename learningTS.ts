// installed globally : ts-node, '@types/node', typescript
// run a ts file by using the command line : npx ts-node .\board.ts

//  What is TypeScript ?
{
  // TypeScript is a programming language developed and maintained by Microsoft.
  // TypeScript is a language extension that adds features to ECMAScript 6. Additional features include:
  // TypeScript offers all of JavaScript’s features, and an additional layer on top of these: TypeScript’s type system.
  // The main benefit of TypeScript is that it can highlight unexpected behavior in your code, lowering the chance of bugs.
}

// The TypeScript Handbook
// Remember: Type annotations never change the runtime behavior of your program.

// Compiler options :
{
  // noEmitOnError : do not generate a .js file in case of type errors. (issue fix : https://github.com/microsoft/TypeScript/issues/23179)
  // noImplicitAny : Turning on the noImplicitAny flag will issue an error on any variables whose type is implicitly inferred as any.
  // strictNullChecks : if true, error is raised when trying to use 'null' and 'undefined' where a concrete value is expected. ('null' anecdote : 'https://en.wikipedia.org/wiki/Null_pointer#History')
}

// extra stuff:
{
  // ts-ignore or ts-expect-error
  // in some cases, while developing, it is necessary to write code that is independent of TS
  {
    // 'ts-ignore' or 'ts-expect-error' ?
    // In some ways // @ts-expect-error can act as a suppression comment, similar to // @ts-ignore. The difference is that // @ts-ignore will do nothing if the following line is error-free.
    // You might be tempted to switch existing // @ts-ignore comments over to // @ts-expect-error, and you might be wondering which is appropriate for future code. While it’s entirely up to you and your team, we have some ideas of which to pick in certain situations.
    //
    // Pick ts-expect-error if:
    // you’re writing test code where you actually want the type system to error on an operation
    // you expect a fix to be coming in fairly quickly and you just need a quick workaround
    // you’re in a reasonably-sized project with a proactive team that wants to remove suppression comments as soon affected code is valid again
    //
    // Pick ts-ignore if:
    // you have a larger project and new errors have appeared in code with no clear owner
    // you are in the middle of an upgrade between two different versions of TypeScript, and a line of code errors in one version but not another.
    // you honestly don’t have the time to decide which of these options is better.
  }

  // const assertion
  // A 'const' assertion tells the compiler to infer the narrowest* or most specific type it can for an expression
  // explanation link : https://stackoverflow.com/questions/66993264/what-does-the-as-const-mean-in-typescript-and-what-is-its-use-case
  // const assertions can only be applied immediately on simple literal expressions. ('as const', check examples)
  // examples :
  {
    // Type '"hello"'
    let x1 = "hello" as const;
    // Type 'readonly [10, 20]'
    let y1 = [10, 20] as const;
    // Type '{ readonly text: "hello" }'
    let z1 = { text: "hello" } as const;

    // Outside of .tsx files, the angle bracket assertion syntax can also be used. :
    // Type '"hello"'
    let x2 = <const>"hello";
    // Type 'readonly [10, 20]'
    let y2 = <const>[10, 20];
    // Type '{ readonly text: "hello" }'
    let z2 = <const>{ text: "hello" };

    // This feature means that types that would otherwise be used just to hint immutability to the compiler can often be omitted.
    // Works with no types referenced or declared.
    // We only needed a single const assertion.
    function getShapes() {
      let result = [
        { kind: "circle", radius: 100 },
        { kind: "square", sideLength: 50 },
      ] as const;
      return result;
    }
    for (const shape of getShapes()) {
      // Narrows perfectly!
      if (shape.kind === "circle") {
        // console.log("Circle radius", shape.radius);
      } else {
        // console.log("Square side length", shape.sideLength);
      }
    }

    // Another thing to keep in mind is that const contexts don’t immediately convert an expression to be fully immutable.

    let arr = [1, 2, 3, 4];
    let foo = {
      name: "foo",
      contents: arr,
    } as const;
    // foo.name = "bar"; // error!
    // foo.contents = []; // error!
    foo.contents.push(5); // ...works!

    // enum-like :
    // export const Colors = {
    //     red: "RED",
    //     blue: "BLUE",
    //     green: "GREEN",
    //   } as const;
    //   // or use an 'export default'
    //   export default {
    //     red: "RED",
    //     blue: "BLUE",
    //     green: "GREEN",
    //   } as const;
  }

  // globalThis : https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-4.html#type-checking-for-globalthis
}

// Everyday Types
{
  // The type names String, Number, and Boolean (starting with capital letters) are legal, but refer to some special built-in types
  // that will very rarely appear in your code. Always use string, number, or boolean for types

  // The Array.isArray() method determines whether the passed value is an Array. - 'Array.isArray([1, 2, 3]);  // true'

  // 'Tuple' :
  {
    // Tuple is a new data type. The tuple is like an array,  So, we can use array methods on tuple, but it defines the length and type structure of it :
    // example1 : 'var employee: [number, string] = [1, "Steve"];'
    // example2 : 'var employeeAndBool: [number, string, ...boolean[]] = [1, "Steve", true, false]' or '... = [1, "Steve", true, false]'
    // tuples types have readonly variants option : 'let pair: readonly [string, number] = ...'
  }

  // Type & Interface
  {
    // A type alias is exactly that - a name for any type.
    // An interface declaration is another way to name an object type
    // differences & examples:
    // 1. extentions :
    // Extending an interface using 'extends' :
    interface Animal {
      name: string;
    }
    interface Bear extends Animal {
      honey: boolean;
    }

    // Extending a type via intersections
    type Car = {
      name: string;
    };
    type Bike = Car & {
      twoWheel: boolean;
    };

    // 2. Adding new fields :
    // Adding new fields to an existing interface
    interface Animal {
      home: string;
    }
    interface Bear extends Animal {
      weight: number;
    }

    // A type cannot be changed after being created : Error :
    // type Window = {
    //   title: string;
    // };
    // type Window = {
    //   ts: TypeScriptAPI;
    // };

    // Error: Duplicate identifier 'Window'.
  }

  // Type Assertions
  {
    // source : https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#type-assertions
    // Sometimes you will have information about the type of a value that TypeScript can’t know about.
    // Error example : '...If this was intentional, convert the expression to 'unknown' first.' // '
    // in case this rule gets too conservative and will disallow more complex coercions that might be valid,
    // you can use two assertions, first to any (or unknown, which we’ll introduce later), then to the desired type.
    // dry example : 'const a = (expr as any) as T;'
  }

  // Literal Types
  // The 'as' const suffix acts like const but for the type system, ensuring that all properties are assigned the literal type instead of a more general version like string or number.
  {
    // example :
    const handleRequest = (a: string, b: "GET" | "POST") => {
      return {
        a,
        b,
      };
    };
    const req = { url: "https://example.com", method: "GET" };
    // handleRequest(req.url, req.method); // Error : Argument of type 'string' is not assignable to parameter of type '"GET" | "POST"'.
    // solution 1:
    // const req = { url: "https://example.com", method: "GET" as "GET" };
    // solution 2:
    handleRequest(req.url, req.method as "GET");
  }

  // Non-null Assertion Operator - (Postfix!) :
  function liveDangerously(x?: number | null) {
    // No error - '..!' operator :
    console.log(x!.toFixed());
  }

  // Enums Ducumantation note :
  // is not a type-level addition to JavaScript but something added to the language and runtime.
  // Because of this, it’s a feature which you should know exists, but maybe hold off on using unless you are sure.
  // link : https://www.typescriptlang.org/docs/handbook/enums.html
}

// Narrowing
{
  // typeof type guards - null :
  {
    // example - TS recognizes null even though it has typeof 'object'
    // function checkNull(a: null | {}) {
    //   if (typeof a === "object") a.toString();
    // }
  }

  // Truthiness narrowing
  // all falsy :
  // console.log(!!(0 || NaN || "" || null || undefined)); // false

  // Equality narrowing :
  {
    function example(x: string | number, y: string | boolean) {
      if (x === y) {
        // We can now call any 'string' method on 'x' or 'y'.
        x.toUpperCase();
        y.toLowerCase();
      }
    }
    // When we checked that x and y are both equal in the above example, TypeScript knew their types also had to be equal.
    // Since string is the only common type that both x and y could take on, TypeScript knows that x and y must be a string in the first branch.
  }

  // The 'in' operator narrowing
  // example :
  {
    type Fish = { swim: () => void };
    type Bird = { fly: () => void };

    function move(animal: Fish | Bird) {
      if ("swim" in animal) {
        return animal.swim();
      }

      return animal.fly();
    }
  }

  // Using type predicates
  // example 1:
  {
    // To define a user-defined type guard, we simply need to define a function whose return type is a type predicate:
    type Fish = { swim: () => void };
    type Bird = { fly: () => void };

    function isFish(pet: Fish | Bird): pet is Fish {
      return (pet as Fish).swim !== undefined;
    }

    // pet is Fish is our type predicate in this example. A predicate takes the form parameterName is Type,
    // where parameterName must be the name of a parameter from the current function signature.

    // example 2:
    class FileSystemObject {
      isFile(): this is FileRep {
        return this instanceof FileRep;
      }
      isDirectory(): this is Directory {
        return this instanceof Directory;
      }
      isNetworked(): this is Networked & this {
        return this.networked;
      }
      constructor(public path: string, private networked: boolean) {}
    }

    class FileRep extends FileSystemObject {
      constructor(path: string, public content: string) {
        super(path, false);
      }
    }

    class Directory extends FileSystemObject {
      children: FileSystemObject[] = [];
    }

    interface Networked {
      host: string;
    }

    const fso: FileSystemObject = new FileRep("foo/bar.txt", "foo");

    if (fso.isFile()) {
      fso.content; // property must exist after type predicates validation
      // console.log(fso instanceof FileSystemObject); // true
      // console.log(fso instanceof FileRep); // true
    } else if (fso.isDirectory()) {
      fso.children;
    } else if (fso.isNetworked()) {
      fso.host;
    }

    // We can use 'this' is Type in the return position for methods in classes and interfaces.
    // When mixed with a type narrowing (e.g. if statements) the type of the target object would be narrowed to the specified Type.
  }

  // The never type :
  // TypeScript will use a never type to represent a state which shouldn’t exist.
  {
    // The never type is assignable to every type; however, no type is assignable to never (except never itself).
    // This means you can use narrowing and rely on never turning up to do exhaustive checking in a switch statement.
    interface Circle {
      kind: "circle";
      radius: number;
    }

    interface Square {
      kind: "square";
      sideLength: number;
    }

    type Shape = Circle | Square;

    function getAreaBoth(shape: Shape) {
      switch (shape.kind) {
        case "circle":
          return Math.PI * shape.radius ** 2;
        case "square":
          return shape.sideLength ** 2;
        default:
          const _exhaustiveCheck: never = shape;
          return _exhaustiveCheck;
      }
    }
    // Try Adding a new member to the Shape union, will cause a TypeScript error:

    interface Triangle {
      kind: "triangle";
      sideLength: number;
    }

    type AllShapes = Circle | Square | Triangle;

    function getAreaAll(shape: AllShapes) {
      switch (shape.kind) {
        case "circle":
          return Math.PI * shape.radius ** 2;
        case "square":
          return shape.sideLength ** 2;
        default:
        // const _exhaustiveCheck: never = shape; // Type 'Triangle' is not assignable to type 'never'.
        // return _exhaustiveCheck;
      }
    }
  }
}

// More on Functions :
{
  // In JavaScript, functions can have properties in addition to being callable
  // If we want to describe something callable with properties, we can write a call signature in an object type:
  // example :
  {
    type DescribableFunction = {
      description: string;
      // a *function* :
      (someArg: number): boolean;
    };
    function doSomething(fn: DescribableFunction) {
      console.log(fn.description + " returned " + fn(6));
    }
  }

  // JavaScript functions can also be invoked with the new operator - Constructors
  // example
  {
    type SomeConstructor = {
      // a constructor *function* :
      new (s: string): {
        s: string;
      };
    };
    function fn(ctor: SomeConstructor) {
      return new ctor("hello");
    }
  }

  // Generic Functions
  // Rule: If a type parameter only appears in one location, strongly reconsider if you actually need it
  // Rule: Always use as few type parameters as possible
  // examples :
  {
    // type parameter basic example :
    {
      function firstElement<Type>(arr: Type[]): Type | undefined {
        return arr[0];
      }
      // By adding a type parameter 'Type' to this function and using it in two places, we’ve created a link between the input of the function (the array) and the output (the return value)
    }

    // type parameter using 'map' example
    {
      function map<Input, Output>(
        arr: Input[],
        func: (arg: Input) => Output
      ): Output[] {
        return arr.map(func);
      }
    }

    // relate two values, but can only operate on a certain subset of values
    // type 'constraint' (אילוץ) example
    // Rule: When possible, use the type parameter itself rather than constraining it
    // example :
    {
      function longest<Type extends { length: number }>(a: Type, b: Type) {
        //  Without the type constraint, we wouldn’t be able to access those properties because the values might have been some other type without a length property.
        if (a.length >= b.length) {
          return a;
        } else {
          return b;
        }
      }

      // longerArray is of type 'number[]'
      const longerArray = longest([1, 2], [1, 2, 3]);
      // longerString is of type 'alice' | 'bob'
      const longerString = longest("alice", "bob");
      // const notOK = longest(10, 100); // Error! Numbers don't have a 'length' property
    }

    // example concat arrays
    {
      function combine<Type>(arr1: Type[], arr2: Type[]): Type[] {
        return arr1.concat(arr2);
      }
      // Normally it would be an error to call this function with mismatched arrays:
      // const arr = combine([1, 2, 3], ["hello"]); //  Type 'string' is not assignable to type 'number'.

      // If you intended to do this, however, you could manually specify Type:
      const arr = combine<string | number>([1, 2, 3], ["hello"]);
    }

    // another example :
    {
      function minimumLength<Type extends { length: number }>(
        obj: Type,
        minimum: number
      ): Type {
        if (obj.length >= minimum) {
          return obj;
        }
        //   else { return { obj.length: minimum } } // Error :     //   Type '{ length: number; }' is not assignable to type 'Type'.
        obj.length = 1000;
        return obj;
      }
    }
  }

  // Function Overloads
  // Always prefer parameters with union types instead of overloads when possible
  // examples
  {
    // different amount of arguments example :
    {
      function makeDate(timestamp: number): Date; // no function implementation yet
      function makeDate(m: number, d: number, y: number): Date; // no function implementation yet
      function makeDate(mOrTimestamp: number, d?: number, y?: number): Date {
        if (d !== undefined && y !== undefined) {
          return new Date(y, mOrTimestamp, d);
        } else {
          return new Date(mOrTimestamp);
        }
      }
      const d1 = makeDate(12345678);
      const d2 = makeDate(5, 5, 5);
      // const d3 = makeDate(1, 3); // Error, No overload expects 2 arguments, but overloads do exist that expect either 1 or 3 arguments.
    }

    //   // diverse input types example :
    {
      function fn(x: boolean): void;
      function fn(x: string): void;
      // function fn(x: boolean) {} // This overload signature is not compatible with its implementation signature.
      function fn(x: string | boolean) {
        return "oops";
      }
    }

    // TypeScript can only resolve a function call to a single overload:
    // another concept example
    {
      function len(s: string): number; // 1st overload
      function len(arr: number[]): number; // 2nd overload
      function len(x: any) {
        return x.length;
      }

      len(""); // OK
      len([0]); // OK
      // len(Math.random() > 0.5 ? "hello" : [0]); // input 'number[] | "hello' is neither the 1st or the 2nd overloads exactly
      // Error : No overload matches this call.

      // changing the implementation to a non-overloaded will solve the issue :
      function lenNoOverLoad(x: any[] | string) {
        return x.length;
      }
      lenNoOverLoad(Math.random() > 0.5 ? "hello" : [0]); // no Error
    }
  }

  // 'this' Type and inheritance
  // in JS you cannot have a parameter called this
  //  TypeScript uses that syntax space to let you declare the type for this in the function body.
  // 'this' is not passed as a parameter when calling the function - it's only there for type safety
  {
    // example :
    {
      interface User {
        id: number;
        admin: boolean;
      }

      const user1 = {
        id: 123,
        admin: false,
      };

      const user2 = {
        id: 124,
        admin: true,
      };

      const users = [user1, user2];

      let admins: User[] = [];

      const filterByAdmin = function (this: User) {
        return this.admin;
      };

      function filterUsers(array: User[], filterByAdmin: () => boolean) {
        return array.filter((u) => {
          return filterByAdmin.call(u);
        });
      }

      admins = filterUsers(users, filterByAdmin);
      //   console.log(admins);
    }
  }

  // Void
  // void is not the same as undefined.

  // object is not Object. Always use object!
  // The special type object refers to any value that isn’t a primitive (string, number, bigint, boolean, symbol, null, or undefined).
  // This is different from the empty object type { },
  // and also different from the global type Object. It’s very likely you will never use Object.

  // unknown
  // The unknown type represents any value. This is similar to the any type, but is safer because it’s not legal to do anything with an unknown value:
  // examples
  {
    // example 1
    {
      function f1(a: any) {
        a.b(); // OK
      }

      function f2(a: unknown) {
        //   a.b(); // Error : Object is of type 'unknown'.
      }
    }
    // example 2
    {
      function safeParse(s: string): unknown {
        return JSON.parse(s);
      }

      // Need to be careful with 'obj'!
      const jsonString = '{ "data" : "someRandomString" }';
      const obj = safeParse(jsonString);
      //   console.log(obj);
    }
  }

  // never
  // Some functions never return a value:
  // The never type represents values which are never observed.
  // examples
  {
    // throw an error example
    {
      function fail(msg: string): never {
        throw new Error(msg);
      }
    }
    // never also appears when TypeScript determines there’s nothing left in a union. example
    {
      function fn(x: string | number) {
        if (typeof x === "string") {
          // do something
        } else if (typeof x === "number") {
          // do something else
        } else {
          x; // has type 'never'!
        }
      }
    }
  }

  // Rest Parameters and Arguments
  {
    // Rest Parameters
    // any type annotation given must be of the form Array<T>or T[], or a tuple type, instead of any
    // example :
    {
      function multiply(n: number, ...m: number[]) {
        return m.map((x) => n * x);
      }
      const a = multiply(10, 1, 2, 3, 4); // 'a' gets value [10, 20, 30, 40]
    }

    // Rest Arguments
    // Note that in general, TypeScript does not assume that arrays are immutable.
    // example :
    {
      const args = [8, 5];

      // 'Math.atan2(a,b) requires 2 arguments :
      // const angle = Math.atan2(...args); // Error : A spread argument must either have a tuple type or be passed to a rest parameter.

      // solution :
      // The best fix for this situation depends a bit on your code, but in general a const context is the most straightforward solution:
      const argsAsConst = [8, 5] as const;

      const angle = Math.atan2(...argsAsConst);
    }
    // example - the push method of arrays takes any number of arguments:
    {
      const arr1 = [1, 2, 3];
      const arr2 = [4, 5, 6];
      arr1.push(...arr2);
      // console.log(arr1); // [ 1, 2, 3, 4, 5, 6 ]
    }
  }

  // Parameter Destructuring
  // example
  {
    type ABC = { a: number; b: number; c: number };
    function sum({ a, b, c }: ABC) {
      console.log(a + b + c);
    }
  }

  // Function Signature : type void
  // link : https://www.typescriptlang.org/docs/handbook/2/functions.html#return-type-void
  // Contextual typing with a return type of void DOESN'T force functions to not return something
  // Using void is safe because it prevents you from accidentally using the return value later on
  // void value is nothing
  // example and meaning
  {
    // Thus, the following implementations of the type () => void are valid:
    // no error is thrown :
    type voidFunc = () => void;

    const f1: voidFunc = () => {
      return true;
    };

    const f2: voidFunc = () => true;

    const f3: voidFunc = function () {
      return true;
    };

    // but, when the return value of one of these functions is assigned to another variable, it will retain the type of void:
    const v1 = f1(); // type is 'void' and unusable

    const v2 = f2(); // type is 'void' and unusable

    const v3 = f3(); // type is 'void' and unusable

    // There is one other special case to be aware of:
    // when a literal function definition has a void return type, that function must not return anything.
    function f4(): void {
      // return true; // Error : Type 'boolean' is not assignable to type 'void'.
    }
    // and same is here
    const f5 = function (): void {
      // @ts-expect-error
      return true;
    };
    // type void didn't work, '@ts-expect-error' blocks the expected error
    // console.log(f5()); // true
    // console.log(typeof f5()); // boolean
  }
  // This behavior exists so that the following code is valid even though Array.prototype.push returns a number
  // and the Array.prototype.forEach method expects a function with a return type of void :
  {
    const src = [1, 2, 3];
    const dst = [0];

    // dst.push(el) returns a number - the new array's length.
    // src.forEach((el) => console.log(dst.push(el))); // logs 2; 3; 4;
  }
}

// Object Types
{
  // destructuring
  // Note that there is currently no way to place type annotations within destructuring patterns. This is because the following syntax already means something different in JavaScript.
  // example and solution :
  {
    function draw({
      shape: Shape,
      xPos: number = 100 /*...*/,
    }: {
      shape: string;
      xPos: number;
    }) {
      // render(shape); // Cannot find name 'shape'. Did you mean 'Shape'?
      // render(xPos); // Cannot find name 'xPos'.
      return { Shape, number };
    }
    const obj = {
      shape: "a triangle",
      xPos: 100,
    };
    // console.log(draw(obj)); // works fine !
  }

  // readonly Properties
  // Properties can also be marked as readonly for TypeScript. While it won’t change any behavior at runtime, a property marked as readonly can’t be written to during type-checking.
  // Using the readonly modifier doesn’t necessarily imply that a value is totally immutable - or in other words, that its internal contents can’t be changed.
  // examples
  {
    // basic example
    {
      interface SomeType {
        readonly prop: string;
      }

      function doSomething(obj: SomeType) {
        // We can read from 'obj.prop'.
        // console.log(`prop has the value '${obj.prop}'.`); // works fine
        // But we can't re-assign it.
        //   obj.prop = "hello"; // Error : Cannot assign to 'prop' because it is a read-only property.
      }
    }
    // example with internal values modification :
    {
      interface Home {
        readonly resident: { name: string; age: number };
      }

      function visitForBirthday(home: Home) {
        // We can read and update properties from 'home.resident'.
        // console.log(`Happy birthday ${home.resident.name}!`);
        home.resident.age++;
      }

      function evict(home: Home) {
        // But we can't write to the 'resident' property itself on a 'Home'.
        // home.resident = {  name: "Victor the Evictor", age: 42, }; //   Cannot assign to 'resident' because it is a read-only property.
      }
    }
  }

  //   Index Signatures
  // Sometimes you don’t know all the names of a type’s properties ahead of time, but you do know the shape of the values.
  // In those cases you can use an index signature to describe the types of possible values
  // examples
  {
    // example
    {
      interface StringArray {
        [index: number]: string;
      }

      const myArray: StringArray = {};
      const missingItem = myArray[1]; // works though undefined, TS assume it is of type string
      myArray[1] = "a"; // works fine
      const stringItem = myArray[1];
    }
    // all properties match their return type
    // example
    {
      interface NumberDictionary {
        [index: string]: number;

        length: number; // ok
        // name: string; // Error : Property 'name' of type 'string' is not assignable to 'string' index type 'number'.
      }
    }
  }

  // Extending Types
  // example
  {
    // interfaces can also extend from multiple types
    // type combine interfaces
    {
      interface Colorful {
        color: string;
      }

      interface Circle {
        radius: number;
      }

      // interface multi expend syntax :
      interface ColorfulCircleInterfaceExtention extends Colorful, Circle {}

      const c1: ColorfulCircleInterfaceExtention = {
        color: "red",
        radius: 42,
      };

      type ColorfulCircleType = Colorful & Circle;

      const c2: ColorfulCircleType = {
        color: "red",
        radius: 42,
      };
    }
  }

  // Generic Object Types
  // generic interface & type declares a type parameter.
  // example and usage
  {
    interface Box<Type> {
      contents: Type;
    }

    interface Apple {
      // ....
    }

    // Same as '{ contents: Apple }'.
    type AppleBox = Box<Apple>;

    function setContents<Type>(box: Box<Type>, newContents: Type) {
      box.contents = newContents;
    }

    // type instead of Interface
    type BoxType<Type> = {
      contents: Type;
    };

    type OrNull<Type> = Type | null;

    type OneOrMany<Type> = Type | Type[];

    type OneOrManyOrNull<Type> = OrNull<OneOrMany<Type>>; // type OneOrManyOrNull<Type> = OneOrMany<Type> | null

    type OneOrManyOrNullStrings = OneOrManyOrNull<string>; // type OneOrManyOrNullStrings = OneOrMany<string> | null
  }

  // The Array Type
  // fundamentals example
  {
    interface Array<Type> {
      /**
       * Gets or sets the length of the array.
       */
      length: number;

      /**
       * Removes the last element from an array and returns it.
       */
      pop(): Type | undefined;

      /**
       * Appends new elements to an array, and returns the new length of the array.
       */
      push(...items: Type[]): number;

      // ...
    }
  }
  // The 'ReadonlyArray' is a special type that describes arrays that shouldn’t be changed. example :
  {
    // similar options :
    const values: ReadonlyArray<string> = ["a", "b"];
    const values2: readonly string[] = ["a", "b"];

    // ...but we can't mutate 'values'.
    // values.push("hello!"); // Error : Property 'push' does not exist on type 'readonly string[]'.
    // values2.push("hello!"); // Error : Property 'push' does not exist on type 'readonly string[]'.

    // readonly can be added to regular type
    let x: readonly string[] = [];
    let y: string[] = [];

    x = y;
    // y = x; // ERROR : The type 'readonly string[]' is 'readonly' and cannot be assigned to the mutable type 'string[]'.
  }

  // Tuple types
  //  Optional tuple elements
  // example
  {
    type Either2dOr3d = [number, number, number?]; // tuple

    function setCoordinate(coord: Either2dOr3d) {
      const [x, y, z] = coord; // z: number | undefined
      // coords length is '2 | 3'
    }
  }
  // Tuples can also have rest elements
  // example
  {
    type StringBooleansNumber = [string, ...boolean[], number];

    // another way :
    const a: number[] = [1, 2, 3];
    const b = [...a, "a"] as const;
  }
}

// Creating Types from Types
// TypeScript’s type system is very powerful because it allows expressing types in terms of other types.
{
  // Generics
  {
    {
      // interface with generic type parameter
      // putting the type parameter on the interface itself lets us make sure all of the properties of the interface are working with the same type.
      // example
      {
        interface GenericIdentityFn<Type> {
          (arg: Type): Type;
        }

        function identity<Type>(arg: Type): Type {
          return arg;
        }

        let myIdentity: GenericIdentityFn<number> = identity;
      }
      // generic type with conditional property
      // example
      {
        interface Lengthwise {
          length: number;
        }

        function loggingIdentity<Type extends Lengthwise>(arg: Type): Type {
          console.log(arg.length); // Now we know it has a .length property, so no more error
          return arg;
        }
      }
      // get a property from an object given its name
      // example
      {
        // 'Key extends keyof Type' - 'Key' parameter is one of the object keys / properties
        function getProperty<Type, Key extends keyof Type>(
          obj: Type,
          key: Key
        ) {
          return obj[key];
        }

        let x = { a: 1, b: 2, c: 3, d: 4 };

        getProperty(x, "a");
        //   getProperty(x, "m"); // Error : Argument of type '"m"' is not assignable to parameter of type '"a" | "b" | "c" | "d"'.
      }

      // creating factories in TypeScript is done by their constructor functions
      // examples
      {
        // basic example
        {
          function create<Type>(c: { new (): Type }): Type {
            return new c();
          }
        }
        // with classes example
        {
          class BeeKeeper {
            hasMask: boolean = true;
          }

          class ZooKeeper {
            nametag: string = "Mikle";
          }

          class Animal {
            numLegs: number = 4;
          }

          class Bee extends Animal {
            keeper: BeeKeeper = new BeeKeeper();
          }

          class Lion extends Animal {
            keeper: ZooKeeper = new ZooKeeper();
          }

          // constructor of a class instance function
          function createInstance<A>(c: new () => A): A {
            return new c();
          }

          // same but with type narrowing
          function createInstanceExtended<
            A extends Animal & { keeper: BeeKeeper | ZooKeeper }
          >(c: new () => A): A {
            return new c();
          }

          createInstance(Lion).keeper.nametag;
          createInstance(Bee).keeper.hasMask;
        }
      }
    }
  }

  // Keyof Type Operator
  // confusing detail - JavaScript object keys are always coerced to a string, example :
  {
    type Mapish = { [k: string]: boolean };
    type M = keyof Mapish; // type M = string | number; that is because it will also accept a number, since 'obj[0]' is always the same as 'obj["0"]'.
  }

  // Indexed Access Types
  // We can use an indexed access type to look up a specific property on another type:
  // example
  {
    type Person = { age: number; name: string; alive: boolean };

    type I1 = Person["age" | "name"]; // type I1 is 'string | number'

    type I2 = Person[keyof Person];

    type key = "age" | "name" | "alive"; // type varaible / alias (כינוי)
    type I3 = Person[key];

    const MyArray = [
      { name: "Alice", age: 15 },
      { name: "Bob", age: 23 },
      { name: "Eve", age: 38 },
    ];

    type Person1 = typeof MyArray[number]; //  type Person1 is { name: string; age: number;}, could be undefined if exceeds length
    type Age = typeof MyArray[number]["age"]; // Age is a number

    const r = MyArray[1000];

    console.log("a", r);
  }

  // Conditional Types
  // like ternary, examples
  {
    type MessageOf<T> = T extends { message: unknown } ? T["message"] : never;

    interface Email {
      message: string;
    }

    interface Dog {
      bark(): void;
    }

    type EmailMessageContents = MessageOf<Email>; // type EmailMessageContents = string

    type DogMessageContents = MessageOf<Dog>; // type DogMessageContents = never

    // another 'toArray' example :
    type ToArray<Type> = Type extends any ? Type[] : never;

    type StrArrOrNumArr = ToArray<string | number>; // type StrArrOrNumArr = string[] | number[]
  }
  // Understanding infer(להסיק) in TypeScript
  // The infer keyword compliments conditional types and cannot be used outside an extends clause
  // Infer allows us to define a variable within our constraint to be referenced or returned.
  // example :
  {
    // the built-in 'ReturnType<T>' is actually : (and using 'infer')
    type ReturnType<T> = T extends (...args: any[]) => infer R ? R : any;

    type Num = ReturnType<() => number>; // type Num = number

    type Str = ReturnType<(x: string) => string>; // type Str = string

    type Bools = ReturnType<(a: boolean, b: boolean) => boolean[]>; // type Bools = boolean[]
  }

  // built-in Extract and Exclude utility types in TypeScript:
  {
    type Extract<T, U> = T extends U ? T : never;
    type Exclude<T, U> = T extends U ? never : T;
  }

  // Mapped Types
  // change type value
  {
    // In this example, OptionsFlags will take all the properties from the type Type and change their values to be a boolean.
    type OptionsFlags<Type> = {
      [Property in keyof Type]: boolean;
    };

    type FeatureFlags = {
      darkMode: () => void;
      newUserProfile: () => void;
    };

    type FeatureOptions = OptionsFlags<FeatureFlags>;
    // type FeatureOptions = {
    //   darkMode: boolean;
    //   newUserProfile: boolean;
    // };
  }
  // - or + : readonly and ?
  // examples
  {
    // example  - Removes 'readonly' attributes from a type's properties
    {
      type CreateMutable<Type> = {
        -readonly [Property in keyof Type]: Type[Property];
      };

      type LockedAccount = {
        readonly id: string;
        readonly name: string;
      };

      type UnlockedAccount = CreateMutable<LockedAccount>;
      //   type UnlockedAccount = {
      //       id: string;
      //       name: string;
      //   }
    }
    // example  - Removes 'optional' attributes from a type's properties
    {
      type Concrete<Type> = {
        [Property in keyof Type]-?: Type[Property];
      };

      type MaybeUser = {
        id: string;
        name?: string;
        age?: number;
      };

      type User = Concrete<MaybeUser>;
      //   type User = {
      //       id: string;
      //       name: string;
      //       age: number;
      //   }
    }
  }
  // re-map keys in mapped types with an as clause(סעיף) in a mapped type:
  //   examples
  {
    // leverage features like template literal types to create new property names from prior ones. examples :
    {
      type Getters<Type> = {
        [Property in keyof Type as `get${Capitalize<
          string & Property
        >}`]: () => Type[Property];
      };

      interface Person {
        name: string;
        age: number;
        location: string;
      }

      type LazyPerson = Getters<Person>;

      // type LazyPerson = {
      //     getName: () => string;
      //     getAge: () => number;
      //     getLocation: () => string;
      // }
    }
    //  filter out keys by producing never via a conditional type, example
    {
      // Remove the 'kind' property
      type RemoveKindField<Type> = {
        [Property in keyof Type as Exclude<Property, "kind">]: Type[Property];
      };

      interface Circle {
        kind: "circle";
        radius: number;
      }

      type KindlessCircle = RemoveKindField<Circle>;

      // type KindlessCircle = {
      //     radius: number;
      // }
    }
  }

  // Template Literal Types
  //  concatenating examples
  {
    type World = "world";
    type Greeting = `hello ${World}`; // type Greeting = "hello world"

    type EmailLocaleIDs = "welcome_email" | "email_heading";
    type FooterLocaleIDs = "footer_title" | "footer_sendoff";

    type AllLocaleIDs = `${EmailLocaleIDs | FooterLocaleIDs}_id`; // type AllLocaleIDs = "welcome_email_id" | "email_heading_id" | "footer_title_id" | "footer_sendoff_id"
    type Lang = "en" | "ja" | "pt";
    type LocaleMessageIDs = `${Lang}_${AllLocaleIDs}`; // type LocaleMessageIDs = "en_welcome_email_id" | "en_email_heading_id" | "en_footer_title_id" | "en_footer_sendoff_id"
    // | "ja_welcome_email_id" | "ja_email_heading_id" | "ja_footer_title_id" | "ja_footer_sendoff_id" | "pt_welcome_email_id"
    // | "pt_email_heading_id" | "pt_footer_title_id" | "pt_footer_sendoff_id"

    type PropEventSource<Type> = {
      eventName: `${string & keyof Type}Changed`;
    };

    function makeWatchedObject<
      Type extends {
        someProperty: string & keyof Type & "data";
      }
    >(obj: Type): Type & PropEventSource<Type> {
      obj.someProperty === "data";
      return {
        ...obj,
        eventName: `${obj.someProperty}Changed`,
      };
    }
  }

  // Uppercase<StringType> , Lowercase<StringType> , Capitalize<StringType> , Uncapitalize<StringType>
  // examples
  {
    {
      type Greeting = "Hello, world";
      type ShoutyGreeting = Uppercase<Greeting>;

      type ASCIICacheKey<Str extends string> = `ID-${Uppercase<Str>}`;
      type MainID = ASCIICacheKey<"my_app">; // type MainID = "ID-MY_APP";
    }
    {
      type Greeting = "Hello, world";
      type QuietGreeting = Lowercase<Greeting>; // type QuietGreeting = "hello, world";

      type ASCIICacheKey<Str extends string> = `id-${Lowercase<Str>}`;
      type MainID = ASCIICacheKey<"MY_APP">; // type MainID = "id-my_app";
    }
    {
      type LowercaseGreeting = "hello, world";
      type Greeting = Capitalize<LowercaseGreeting>; // type Greeting = "Hello, world"
    }
    {
      type UppercaseGreeting = "HELLO WORLD";
      type UncomfortableGreeting = Uncapitalize<UppercaseGreeting>; // type UncomfortableGreeting = "hELLO WORLD"
    }
  }
}
