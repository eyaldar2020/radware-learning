class Hero {
  constructor(heroName) {
    this.heroName = heroName;
  }
  logNameRegular() {
    console.log(this.heroName);
  }
  logNameArrow = () => {
    console.log(this.heroName);
  };
}

const batman = new Hero("Batman");

const spiderman = {
  heroName: "not batman but spiderman",
  localLogNameRegular: batman.logNameRegular,
  localLogNameArrow: batman.logNameArrow,
};

// with Regular Functions, when the method is separated from the object, it's object belonging changes dynamically:
// example 1
setTimeout(batman.logNameRegular, 1000); // after 1 second logs "undefined"
//
// example 2
spiderman.localLogNameRegular(); // logs "not batman but spiderman"

// with Arrow functions however, the method defined binds 'this' lexically to the class instance :
// example 1:
setTimeout(batman.logNameArrow, 1000); // after 1 second logs "batman"
//
// example 2
spiderman.localLogNameArrow(); // logs "Batman"
