# README

Learn & Practice JS fundamentals

### Subjects

- context(this)
- bind, call, apply
- arrow functions
- hoisting
- memoizing
- scope, closures
- currying functions
- es6, var let const, spread, rest, destructuring operators
- classes, instances, inheritence, getters and setters

-

### Exercises

- calculate factorial effectively :

  - calculate the factorial recursively.
  - make it effective, so each value already called would be returned fast.
  - example :

    getFactorial(523); // could take 1-2 seconds

    getFactorial(523); // immediate

-
