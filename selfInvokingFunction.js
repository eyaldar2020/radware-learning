// first example :

// self-invoking function as a property - Object and Class familiarities :
// Everything concerning the main navigation in here:
var prop = 100;
var mainNavigation = (function (prop) {
  var currentIndex = 0;
  var maxIndex = 10;

  var moveTo = function (index) {
    if (index < 0 || index > maxIndex) return;
    currentIndex = index;
    // Animate to page number [currentIndex] here
  };

  // like props :
  console.log("prop", prop);

  return {
    next: function () {
      moveTo(currentIndex + 1);
    },
    previous: function () {
      moveTo(currentIndex - 1);
    },
    moveTo: moveTo,
    printCurrentIndex: function () {
      console.log(currentIndex);
    },
    // like-a getter
    currentIndex,
  };
})(prop);

const { next, previous, moveTo, printCurrentIndex } = mainNavigation;
next();
next();
next();
printCurrentIndex(); // 3

mainNavigation.moveTo(8);
printCurrentIndex(); // 8
console.log(mainNavigation.currentIndex); // 0 // initial value

// features :
// feels like a hook : execute, return and maintain properties in a seperated enviroment
// can receive 'props', from another upper/outside scope
// return fields and like-methods of an object or a class. requires like-a getter to ask for properties.

//

// second example :

(function (originalFunction) {
  // originalFunction is Math.round
  console.log("here"); // logs at function self-invoking execution

  // newFunc is created at function self-invoking execution
  newFunc = function (number) {
    var result = originalFunction.call(null, number); // simple Function.prototype.call()

    console.log(number + " has been rounded to " + result);
    return result;
  };
})(Math.round);

newFunc(3.5);
