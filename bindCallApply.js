// Bind, Call, Apply

const Ayal = {
  name: "ayal",
};

const Eyal = {
  name: "eyal",

  callName(a, b) {
    console.log(a, b);
    console.log(this.name);
  },
};

Eyal.callName();

// all the same :
Eyal.callName.call(Ayal, a, b);
Eyal.callName.apply(Ayal, [a, b]);
const callNameAyal = Eyal.callName.bind(Ayal, a, b); // creates a function : the object param uses the chosen function.
callNameAyal();
