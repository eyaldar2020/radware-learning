// Scope And Closures :

// CallBack scope example - setTimeOut:
// 1. global variable
function countSeconds(howMany) {
  // var is defined once in this scope, and being used by all of the 'setTimeout's
  for (var i = 1; i <= howMany; i++) {
    setTimeout(function () {
      console.log(i);
    }, i * 1000);
  }
}
// countSeconds(3); // Incorrect

// 2. local variable
function countSeconds2(howMany) {
  for (let i = 1; i <= howMany; i++) {
    // let is defined everytime in this scope, and being used by each of the 'setTimeout's individually
    setTimeout(function () {
      console.log(i);
    }, i * 1000);
  }
}
// countSeconds2(3); // Correct

// 3. another way to solve - create local variable out of global variable :
function countSeconds3(howMany) {
  for (var i = 1; i <= howMany; i++) {
    (function () {
      var currentI = i;
      setTimeout(function () {
        console.log(currentI);
      }, currentI * 1000);
    })();
  }
}
// countSeconds3(3); // Correct

// 4. wrap the loop's inside with a self-invoking func to create local scope :

function countSeconds4(howMany) {
  for (var i = 1; i <= howMany; i++) {
    //  self-invoking func that receives 'i' as a parameter
    (function (i) {
      // in this scope, i is the same i that passed as a parameter when the function was called
      // setTimeout is scoped inside the self-invoking :
      setTimeout(() => console.log(i), i * 1000);
    })(i); // i is passed to the self-invoking scope
  }
}
// countSeconds4(3); // Correct

//

// simple example of scoping 'var',
// almost like 'let' (though let isn't hoisted)
(function () {
  console.log(a); // var hoisting : undefined
  var a = 0;
  console.log(a); // 0
});
// (); // move this up row to execute
// console.log(a); // outside the function - Error : 'a' is not defined
