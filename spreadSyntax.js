// spread Array
// combine few Arrays
const array1 = [1, 3, 2];
const array2 = [4, 5, 6];

const bothArrays1 = array1.concat(array2);
const bothArrays2 = [...array1, ...array2];

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;

  for (let i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}
console.log(
  "spread technique is similar to concat : ",
  arraysEqual(bothArrays1, bothArrays2)
); // true
console.log(); // console space

// create a new array from the existing one and add a new item to the end (without mutating the existing Array)
// Notice : Spread allows you to make a shallow copy of an array or object :
// meaning that any top level properties will be cloned, but nested objects will still be passed by reference
// 1. create a shallow copy and use it :
let array3 = [
  {
    a: 1,
  },
  {
    b: 2,
  },
];

const itemToAdd = {
  c: 3,
};

let newArray3 = [...array3, itemToAdd];
console.log("spread and add item : ", newArray3); // logs [ { a: 1 }, { b: 2 }, { c: 3 } ]
console.log(); // console space

// 2. demonstrate that the copy is not creating a deep copy to nested objects :
array3[0].a = 10;
console.log(
  "newArray3 is changed after modifying the `old array3` : ",
  newArray3
); // [ { a: 10 }, { b: 2 }, { c: 3 } ]
console.log(); // console space

// creating an array from a string:
const string = "hello";

const stringArray = [...string];
console.log(`array from a string '${string}': `, stringArray); // 'hello' & [ 'h', 'e', 'l', 'l', 'o' ]
console.log(); // console space

//
// spread Objects
// shallow copy an object by spreading it into a new one. unnested items will be copied and not referenced :
const originalObject = { enabled: true, darkMode: false };
const secondObject = { ...originalObject };

console.log("simple copy : ", secondObject); // { enabled: true, darkMode: false }
console.log(); // console space

originalObject.enabled = false;
console.log("modified unnested object does not influence the new object : ");
console.log(originalObject); // { enabled: false, darkMode: false }
console.log(secondObject); // { enabled: true, darkMode: false }
console.log(); // console space

// deep copy of nested objects requires pointing and specifically copying each nested object individually :
const user = {
  id: 3,
  name: "Ron",
  organization: {
    name: "Parks & Recreation",
    city: "Pawnee",
  },
};

// copy object without mutation
const deepCopyUser = { ...user, organization: { ...user.organization } };
console.log("deepCopyUser", deepCopyUser);
console.log(); // console space

// modify object without mutation
const updatedUser = { ...user, organization: { position: "Director" } };
console.log("updatedUser", updatedUser);
console.log(); // console space

// shllow copy of an object mutation, nested object reference
const shallowUserCopy = { ...user };
shallowUserCopy.organization.name = "new name for all";
console.log("shallow copy user : ");
console.log(user);
console.log(shallowUserCopy);
console.log(); // console space

// copy and modify nested object without mutation :
const copyAndModifyUser = {
  ...user,
  organization: { ...user.organization, position: "Director" },
};
console.log("copyAndModifyUser", copyAndModifyUser);
console.log(); // console space

//
// Spread with Function Calls
function multiply(a, b, c) {
  return a * b * c;
}

multiply(1, 2, 3); // 6

const numbers = [1, 2, 3];

multiply(...numbers); // 6

// same as :
multiply.apply(null, [1, 2, 3]);
