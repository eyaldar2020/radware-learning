// Regular Function can easily construct objects :
function Car(color) {
  this.color = color;
}
const redCar = new Car("red");
redCar instanceof Car; // => true
console.log(redCar); // logs "Car { color: 'red' }"

// A consequence of 'this' resolved lexically is that an arrow function cannot be used as a constructor
// it is an Error, even if no 'this' is being used
const Bus = (color) => {
  this.color = color;
};
const blueBus = new Bus("red"); // Error
console.log(blueBus); // logs "TypeError: Bus is not a constructor"
