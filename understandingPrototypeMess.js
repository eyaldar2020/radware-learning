// classes, instances, inheritence, getters and setters

// OOP - Object Oriented Programming

//  JS don't really have Classes? but it is possible to use JavaScript like a class-based language
//  'Prototype-based programming is a style of object-oriented programming in which behaviour reuse
//   (known as inheritance) is performed via a process of reusing existing objects that serve as prototypes.'

//  'The most important difference between class- and prototype-based inheritance is that a class defines a type
//   which can be instantiated at runtime, whereas a prototype is itself an object instance.'

//  abstraction : hide the details, show the essentials

// creating objects using functions: no real difference between the 2 ways :
function createObjByReturnIt() {
  return {
    x: 1,
  };
}
const obj = createObjByReturnIt();

// console.log(obj.constractor); // undefined
// console.log(obj);

function CreateNewObj() {
  this.x = 1;
  return this; // return 'this' is the actual action, but it is not necessary to hard code
}
const newObj = new CreateNewObj();
// console.log(typeof newObj); // CreateNewObj { x: 1 }
// console.log(newObj);

// Building Constractors :
let x1 = {}; // let x = new Object()
let x2 = ""; // let x = new String()
let x3 = true; // let x = new Boolean(true)
let x4 = 0; // let x = new Number(0)

// JS is built on :
// primitives (Value Types) - Number, String, Boolean, Symbo;, undefined, null
// objects - Object, Function, Array

// the difference is : primitives variables stores value, but object value is referenced to somewhere else in the memory,
// and if location value changes, it impacts anywhere this location is used

// so you could say it is all a part of a single class

let x = 10;
let y = x;
x = 20;
// console.log(y); // 10

let obj2 = { val: 10 };
const increase = (obj) => obj.val++;
increase(obj2);
// console.log(obj2); // 11

const obj3 = { a: 1, b: 2 };

const keys = Object.keys(obj3);
// console.log(keys); // prints to the console array of the object keys

//

// null.

// const a = {
//   b: {
//     c: 1,
//   },
// };

// const T = function () {
//   this.x = 1;
// };
// let ClassZ = class Z {
//   y = 1;

//   constructor() {
//     this.x = 1;
//   }

//   static displayName = "Point";
//   getDetails() {
//     return "hey";
//   }
// };

// ClassZ.prototype.h = 10;

// const z = new ClassZ();
// console.log(Object.getPrototypeOf(z));
// // console.log(z.prototype); // undefined
// console.log(z.__proto__);
// console.log("ClassZ proto", Object.getPrototypeOf(ClassZ));
// console.log(ClassZ.prototype.constructor);
// console.log(ClassZ.constructor);
// console.log(Object.getPrototypeOf({}));
// console.log(
//   Object.getPrototypeOf(Object.getPrototypeOf(Object.getPrototypeOf({}))) // TypeError: Cannot convert undefined or null to object
// );

// const t = new T();
// t.r = 100;
// T.prototype.xr = 100;
// console.log(t);
// // console.log(Object.getPrototypeOf(Z));
// console.log(Object.getPrototypeOf(t));
// console.log(Object.getPrototypeOf(T));
// console.log(t.constructor);
// console.log(T.constructor);
// console.log(T.prototype.constructor);
// // console.log();
// console.log(Object.getPrototypeOf(Object.getPrototypeOf(T)));
// console.log(
//   Object.getPrototypeOf(Object.getPrototypeOf(Object.getPrototypeOf(T)))
// );

// const l = new T();
// T.prototype.x = 1;
// console.log(l.x);

// Only constructor functions have prototypes.

class A {
  constructor() {
    this.a = 10;
  }
}

const a = new A();
// console.log(a); // A { a: 10 }

// console.log(Object.getPrototypeOf(a) === A.prototype); // true

// console.log(a.x); // undefined
// A.prototype.x = 20;
// console.log(Object.getPrototypeOf(a)); // { x: 20 }
// console.log(A.prototype); // { x: 20 }
// console.log(a.x); // 20`
// console.log(a.prototype); // undefined

console.log(typeof Object); // function
console.log(Object.prototype); // {}
console.log(typeof Object()); // object

Object.prototype.aaaa = 1000;
console.log(A.aaaa);
console.log({}.aaaa);

console.log(Object.getPrototypeOf(A)); // {}

console.log("here");
console.log(A.prototype.constructor);
console.log(A.constructor);
console.log(Object.getPrototypeOf(Object));
console.log(Object.getPrototypeOf(A.prototype.constructor));

console.log(Object.prototype);
console.log(Object.getPrototypeOf(Object.getPrototypeOf(A)));

function doSomething() {}
console.log(doSomething.prototype);
console.log(doSomething.prototype);
console.log(doSomething.prototype);
console.log(doSomething.prototype);
//  It does not matter how you declare the function; a
//  function in JavaScript will always have a default
//  prototype property — with one exception: an arrow
//  function doesn't have a default prototype property:
// example
// const doSomethingFromArrowFunction = () => {};
// console.log(doSomethingFromArrowFunction.prototype);
// console.log("doSomethingFromArrowFunction.prototype");

const s = "s";

const h = {};
console.log(h.aaaa);

const arr = [];
console.log(arr.aaaa);
console.log(arr.prototype);
console.log(Object.getPrototypeOf(arr));
console.log(Object.getPrototypeOf(Object.getPrototypeOf(arr)));

function f() {
  return 2;
}
console.log(Object.getPrototypeOf(h));

console.log(f.aaaa);
console.log(s.aaaa);
console.log("here now");

String.prototype.ssss = 100;
console.log(Object.getPrototypeOf(h));
console.log(Object.getPrototypeOf(arr));

// Object.getPrototypeOf(arr).prototype.rrrr[0] = 9999;
Array.prototype.rrr = 999;
Array.prototype.rrrr = 9999;
console.log("Object.getPrototypeOf(arr");
console.log(Object.getPrototypeOf(arr));
console.log(Object.getPrototypeOf(Object.getPrototypeOf(arr)));

console.log(Object.getPrototypeOf(Object.getPrototypeOf(arr)));
// console.log(Object.getPrototypeOf(Object.getPrototypeOf(s)));
// console.log(Object.getPrototypeOf(s));

const c = {};
console.log(Object.getPrototypeOf(c));
console.log(Object.getPrototypeOf(f));

const v = {};
console.log(Object.getPrototypeOf(v));

Object.setPrototypeOf(v, Array.prototype);
console.log("Object.getPrototypeOf(v)");
console.log(Object.getPrototypeOf(v));
console.log(Object.getPrototypeOf(v));
console.log(Object.getPrototypeOf(Object.getPrototypeOf(v)));
console.log(
  Object.getPrototypeOf(Object.getPrototypeOf(Object.getPrototypeOf(v)))
);

class CC {}
console.log(Object.getPrototypeOf(CC));
console.log(Object.getPrototypeOf(Object.getPrototypeOf(CC)));
console.log(CC.prototype);
Object.setPrototypeOf(CC.prototype, Array.prototype);
Object.setPrototypeOf(CC, Array.prototype);
console.log(Object.getPrototypeOf(CC));
console.log(Object.getPrototypeOf(Object.getPrototypeOf(CC)));

console.log(CC.prototype);
const cc = new CC();
CC.prototype.lll = 888;
console.log(Object.getPrototypeOf(cc));
console.log(Object.getPrototypeOf(Object.getPrototypeOf(cc)));

console.log(cc.aaaa);

// so I ran into a problem :
// Object -> Array -> CC class -> cc
// Object -> CC class -> cc

// that means setting CC prototype ('Object.setPrototypeOf(CC, Array.prototype);')
// is not enough for its decendent to know a change was made in the prototype chain
// what is the solution? maybe constructor

console.log(Object.getPrototypeOf(cc));
console.log(Object.getPrototypeOf(CC));
console.log(Object.getPrototypeOf(Array));
console.log(cc.aaaa);
console.log(cc.rrr);

console.log(Object.prototype.constructor);
// console.log({}.prototype.constructor); // not a function?

console.log(CC.prototype.constructor);
console.log(Object.getPrototypeOf(Object.getPrototypeOf(Function)));

console.log(Number.toString());
console.log(Number.isNaN());
console.log(Number());
console.log(Number.constructor);
console.log(Number(1).constructor);

const kkkk = new String("kkkk");
console.log(kkkk);
console.log(typeof kkkk);
kkkk.y = 5;

const kkk = "lala";
kkk.s = 10;

console.log(kkk);
console.log(kkkk.valueOf);
console.log(typeof kkkk.valueOf() === typeof kkkk.toString());
// console.log(typeof kkkk.toString());

const toString = Object.prototype.toString;
console.log(toString.call(new Date())); // [object Date])
console.log(toString.call(new String("dasfdsa"))); // [object String])
console.log(toString.call(Math)); // [object Math])
console.log(toString.call(undefined)); // [object Undefined])
console.log(toString.call(null)); // [object Null]

// console.log(null.toString());
// console.log(new Date().toString());
// console.log(new String("a").toString());
// console.log(Math.toString());
// console.log(undefined.toString());
// console.log();

// Object.getPrototypeOf()
console.log(
  toString.call(
    Object.getPrototypeOf(Object.getPrototypeOf(new String("dasfdsa")))
  )
);
console.log(
  Object.getPrototypeOf(Object.getPrototypeOf(new String("dasfdsa")))
);
console.log(toString.call(null)); // [object Null]

//
//
//
//

//
//
//

//
//
//

//
//
//

//
//
//

//
//

const d = Object.create(null);
console.log(d.hasOwnProperty); // undefined, because d doesn't inherit from Object.prototype
const dd = Object.create({});
console.log(dd.hasOwnProperty); // because dd inherits from Object.prototype

// The Object.create() method creates a new object, using an existing object as the prototype of the newly created object.
const dddd = {};
dddd.d = 100000;
const ddd = Object.create(dddd);
console.log(Object.getPrototypeOf(ddd)); // {d - 100000}
console.log("here", dddd.prototype);
console.log(dddd);
console.log(Object.getPrototypeOf(Object.getPrototypeOf(ddd))); // [Object: null prototype] { aaaa: 1000 }

class ClassWithStaticMethod {
  static staticProperty = "someValue";
  static staticMethod() {
    return "static method has been called.";
  }
  static {
    console.log("Class static initialization block called");
  }
}
console.log(ClassWithStaticMethod);
console.log(ClassWithStaticMethod.prototype);
console.log(ClassWithStaticMethod.staticMethod);
console.log(ClassWithStaticMethod.staticMethod());

const z = {};
console.log(z.constructor);

// let val = null;
// val.constructor = 1; //TypeError: val is null

let val = "abc";
val.constructor = Number; //val.constructor === String
console.log(val.constructor);

let aa = String;
console.log(aa.constructor);
aa.constructor = Number;
console.log(aa.constructor);
aa.constructor === String; // true
aa instanceof String; //false
aa instanceof Array; //true

class Graph {
  constructor() {
    this.vertices = [];
    this.edges = [];
  }
}

Graph.prototype.addVertex = function (v) {
  this.vertices.push(v);
};

const gg = new Graph();
// const g = new Graph();
const g = {};
g.a = { a: 1 };
console.log(g.prototype); // undefined
console.log(Object.getPrototypeOf(g));

const lll = Object.create(g);

console.log(g.prototype); // undefined
console.log(Object.getPrototypeOf(lll));
console.log(Object.getPrototypeOf(Object.getPrototypeOf(lll)));

g.b = { b: 100 };
console.log(lll.b);
console.log(g);

console.log(Graph.constructor.name);
console.log(gg.constructor.name);
console.log(Object.getPrototypeOf(gg.constructor.name));
console.log(Object.getPrototypeOf(gg));
console.log(Object.getPrototypeOf(gg).constructor);
console.log(Object.getPrototypeOf(gg).constructor.name);

// console.log(gg.prototype.constructor);
console.log(Graph.prototype.constructor);
// console.log(gg.constructor.addVertices);
// console.log(Graph.constructor.addVertices);

function Parent() {
  /* ... */
}
function CreatedConstructor(a = 10) {
  this.a = a;

  /* ... */
}

CreatedConstructor.prototype = Object.create(Parent.prototype);
CreatedConstructor.prototype.constructor = CreatedConstructor; // sets the correct constructor for future use

CreatedConstructor.prototype.create = function create(a) {
  console.log("here", a);
  // console.log(this.constructor());
  return new this.constructor(a);
};

const cons = new CreatedConstructor()
  .create(5)
  .create(6)
  .create()
  .create()
  .create(100); // it's pretty fine

const cons2 = new CreatedConstructor().create("a");
console.log("cons", cons);
console.log(cons2);
cons = cons.create(1000);
console.log(cons);
console.log(cons.create(1000));
