// destructuring operator - Array destructure :
const nestedArray = [1, 2, [3, 4], 5];
const [one, andTwo, [andThree, andFour], andFive] = nestedArray; // Destructure nested items

// notice : naming is custom when destructuring array
console.log(
  "destracture an array and nested array with custom naming : ",
  one,
  andTwo,
  andThree,
  andFour,
  andFive
); // logs '1 2 3 4 5'
console.log(); // console space
