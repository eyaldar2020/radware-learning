// currying functions
//  great explanation : 'https://www.youtube.com/watch?v=I4MebkHvj8g'

// Currying is an advanced technique of working with functions : basically, a multy function sort of structure
// it devides a multy arguments or execution steps into a few chained functions that executes synchronically

// examples:
// 1 : basic
const multiply = (x, y) => x * y;
const curriedMultiply = (x) => (y) => x * y;

multiply(2, 3); // 6
curriedMultiply(2); // (2) & y => x * y
curriedMultiply(2, 3); // 6

const timesTen = curriedMultiply(10); // returns func that accept 1 arg and multiply it by 10
console.log(timesTen(8)); // 80

// 2 : from inside out principle
const a =
  (fn) =>
  (...args) => {
    console.log("1. do...");
    return fn(...args);
  };

const b =
  (fn) =>
  (...args) => {
    console.log("2. args : ", args[0]);
    return fn(...args);
  };

const c = (...args) => {
  console.log("3. args again : ", args[0]);
};

const insideOut = b(c);
const insideOutTop = a(insideOut);
insideOutTop("some args");

// 3 : advanced
const curry = (fn) => {
  // console.log(here) // logs 'here' only once
  return (func = (...args) => {
    if (fn.length !== args.length) {
      // fn.length is parameters count
      // 1st : args.length = 1, args = [4]
      // 2nd : args.length = 2, args = [4 , 5]
      return func.bind(null, ...args);
    }
    // 3rd : args.length = 3, args = [4 , 5, 6]
    return fn(...args);
  });
};

const total = (a, b, c) => a + b + c; // fn.length = 3

const curriedTotal = curry(total); // returns the inner function
console.log(curriedTotal(4)(5)(6));
