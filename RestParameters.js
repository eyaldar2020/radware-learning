// Rest Parameters
// The syntax appears the same as spread (...) but has the opposite effect

// example :
//  All the arguments passed to the restTest function are now available in the args array:
function restTest(...args) {
  console.log(args);
}
restTest(1, 2, 3, 4, 5, 6); // Output:  [1, 2, 3, 4, 5, 6]

// In older code, the arguments variable could be used to gather all the arguments passed through to a function:
// arguments is 'Array -like object' - it is not a true array and cannot use methods like map and filter without first being converted to an array
function testArguments() {
  console.log(typeof arguments); // object
  console.log(arguments);
  console.log(arguments[0]); // output : 'how'

  // Error :
  // console.log(arguments.map((x) => x)); // 'arguments.map is not a function'

  // convert arguments into Array instances :
  console.log(sortArgs(arguments).map((x) => x));
}

function sortArgs(...args) {
  // Array.from simply convert Array-like or Iterable objects into Array instances :
  return Array.from(arguments).sort(function (a, b) {
    return a - b;
  });
}
testArguments("how", "many", "arguments"); // Output: Arguments(3) - { '0': 'how', '1': 'many', '2': 'arguments' } // 'Array -like object'
