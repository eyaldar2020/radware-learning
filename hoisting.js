// Functions :

// hoisting allows using a function before it is declared in the code :
a(); // executes 'a()' : logs 'hoisting'

function a() {
  console.log("hoisting");
}

// a function nested in a function IS NOT initialized before execution :
function b() {
  function c() {
    console.log("nested function");
  }
}
// c(); // "ReferenceError : c is not defined"

// but, inside the function, hoisting is working once again :
function d() {
  return e();

  function e() {
    console.log("inner hoisting");
  }
}
d(); //  logs 'inner hoisting'
//

// Var and other cases :

// The default initialization of the var is undefined, yet it will initialize before executing the code :
console.log(v); // initialized but not declared : logs 'undefined'
var v = 10; // Initialization and declaration.
console.log(v); // logs '10'

// let and const & class decleration :
// The variables are created when their containing Lexical Environment is instantiated but may not be accessed in any way until the variable’s LexicalBinding is evaluated.
// url : 'https://stackoverflow.com/a/31222689/157247'

// var, let and const example :
function variablesHoisting(arr) {
  //i is known here but undefined
  //j is not known here

  console.log(i);
  //   console.log(j); // throws an Error : "ReferenceError: j is not defined"

  for (var i = 0; i < arr.length; i++) {
    //i is known here
  }

  //i is known here
  //j is not known here

  console.log(i);
  //   console.log(j); // throws an Error : "ReferenceError: j is not defined"

  for (let j = 0; j < arr.length; j++) {
    //j is known here
  }

  //i is known here
  //j is not known here

  console.log(i);
  //   console.log(j); // throws an Error : "ReferenceError: j is not defined"
}
// variablesHoisting([1, 2]); // logs 'undefined'; '2'; '2'
//
