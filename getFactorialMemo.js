// nice example of memoization function :
// const memoize = (func) => {
//   const results = {};
//   return (...args) => {
//     const argsKey = JSON.stringify(args);
//     if (!results[argsKey]) {
//       results[argsKey] = func(...args);
//     }
//     return results[argsKey];
//   };
// };

const memoize = require("memoizee");

function runMemoUsingLibrary() {
  const memoizedFactorialFunc = memoize(getFactorial);

  function getFactorial(n) {
    if (n > 1) {
      return getFactorial(n - 1) * n;
      // return memoizedFactorialFunc(n - 1) * n; // use this in order to moemoize all the options up to the max parameter that was called.
    }
    return n;
  }

  memoizedFactorialFunc(2222); // highest number parameter
  memoizedFactorialFunc(2001);
  memoizedFactorialFunc(2002);
  memoizedFactorialFunc(2003);
  memoizedFactorialFunc(2004);
  memoizedFactorialFunc(2005);
  memoizedFactorialFunc(2006);
  memoizedFactorialFunc(2007);
  memoizedFactorialFunc(2008);
  memoizedFactorialFunc(2009);
  memoizedFactorialFunc(2010);
  memoizedFactorialFunc(2011);
  memoizedFactorialFunc(2012);
  memoizedFactorialFunc(2013);
  memoizedFactorialFunc(2014);
  memoizedFactorialFunc(2111);
  memoizedFactorialFunc(2112);
  memoizedFactorialFunc(2113);
  memoizedFactorialFunc(2114);
  memoizedFactorialFunc(2115);
  memoizedFactorialFunc(2116);
  memoizedFactorialFunc(2117);
  memoizedFactorialFunc(2118);

  // all the same :
  memoizedFactorialFunc(2118);
  memoizedFactorialFunc(2118);
  memoizedFactorialFunc(2118);
  memoizedFactorialFunc(2118);
  memoizedFactorialFunc(2118);
  memoizedFactorialFunc(2118);
  memoizedFactorialFunc(2118);
  memoizedFactorialFunc(2118);
}

function runMemoMultiNum() {
  let results = {
    max: 1,
    1: 1,
  };

  function getFactorial(n) {
    if (n > results.max) {
      let result = getFactorial(n - 1) * n;
      results[n] = result;
      return result;
    }
    return results[results.max.toString()];
  }

  function getFactorialWithMemory(n) {
    if (results[n]) return results[n];

    let result = getFactorial(n - 1) * n;

    results[n] = result;
    results.max = n;
    return result;
  }

  getFactorialWithMemory(2000);
  getFactorialWithMemory(2001);
  getFactorialWithMemory(2002);
  getFactorialWithMemory(2003);
  getFactorialWithMemory(2004);
  getFactorialWithMemory(2005);
  getFactorialWithMemory(2006);
}

function runNoMemo() {
  function getFactorial(n) {
    if (n > 1) {
      return getFactorial(n - 1) * n;
    }
    return n;
  }

  getFactorial(2000);
  getFactorial(2000);
  getFactorial(2000);
  getFactorial(2000);
  getFactorial(2000);
  getFactorial(2000);
  getFactorial(2000);
}

function runMemoSingleNum() {
  let results = {};

  function getFactorial(n) {
    if (n > 1) return getFactorial(n - 1) * n;
    return 1;
  }

  function getFactorialWithMemory(n) {
    if (results[n]) return results[n];

    results[n] = getFactorial(n - 1) * n;

    return results[n];
  }

  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
  getFactorialWithMemory(2000);
}

function performaceSensitiveFunc() {
  const start = performance.now();
  // runMemoMultiNum();
  runMemoSingleNum();
  // runNoMemo();
  // runMemoUsingLibrary();
  const duration = performance.now() - start;
  console.log("duration", duration);
}

performaceSensitiveFunc();
