// CONTEXT : 'this' Arrow VS Functions

const Asaf = {
  name: "asaf",
  nameThis: this.name,

  arrow: () => {
    return console.log(this.name); // logs to the console 'undefined'
  },
  arrowInsideFunction() {
    return () => {
      console.log(this.name);
    }; // logs to the console 'name'
  },
  function() {
    console.log(this.name);
  }, // logs to the console 'name'
};

// 'this' is dynamic
// Arrow function  doesn't have it's own 'this'. it is inherited from upper function if exists
// in Regular Function 'this' depends on how the function invoked :
// -  'independent' function / simple invocation refers to global object (window)
// -  During a method invocation the value of this is the object that owns the method
// -  During an indirect invocation (using 'call', 'bind', 'apply'),  the value of this equals to the first argument
// -  During a constructor invocation using new keyword this equals to the newly created instance

// Example : independent function inside independent function : still refers to the global object (window)
function a() {
  const that = this;
  function s() {
    return that === this;
  }
  return s();
}
// a()

// Example : arrow function inherits the method's 'this', equals 'myObject'
const myObject = {
  myMethod(items) {
    console.log(this); // logs myObject
    const callback = () => {
      console.log(this); // logs myObject
    };
    items.forEach(callback);
  },
};
// myObject.myMethod([1, 2, 3]);
