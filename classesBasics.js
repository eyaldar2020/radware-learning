// classes basics

// statics
// A static method is a function that is bound to the class, not an object.

//

// getters And setters
// An important part of encapsulation is that data (object properties) should not be directly accessed or modified from outside the object.

// get & set
// https://www.programiz.com/javascript/getter-setter

// example 1: in an object
(function example1() {
  const student = {
    firstName: "Monica",

    // accessor property(getter)
    get getName() {
      return this.firstName;
    },

    //accessor property(setter)
    set changeName(newName) {
      this.firstName = newName;
    },
  };

  // trying to access as a method will cause an error :
  // console.log(student.getName()); // error

  console.log(student.firstName); // Monica
  // accessing getter methods
  console.log(student.getName); // Monica

  // change(set) object property using a setter
  student.changeName = "Sarah";
  console.log(student.firstName); // Sarah
})();

//  example 2 : in a Class
(function example2() {
  class User {
    constructor(name) {
      this._name = name;
    }

    get name() {
      return this._name;
    }

    set name(newName) {
      this._name = newName;
    }
  }

  const eyal = new User("Eyal");

  console.log(eyal);
  console.log(eyal._name);
  console.log(eyal.name);
})();

// example 3 - private properties in a Class :
// there is no native support for private properties with ES6 classes.
(function example3() {
  class HideProperties {
    // note : get & set functions will be re-created for each instance, instead of being Class methods
    constructor(property) {
      let _property = property;

      this.getProperty = function getProperty() {
        return _property;
      };

      this.setProperty = function (property) {
        _property = property;
      };
    }
  }

  const hidden = new HideProperties("original property");
  console.log(hidden._property); // undefined
  console.log(hidden.getProperty());
  hidden.setProperty("newProperty");
  console.log(hidden.getProperty());
  hidden.setProperty("newerProperty");
  console.log(hidden.getProperty());
})();
